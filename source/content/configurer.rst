Configuration de Gitlab
=========================

Il existe différentes façons de créer un projet ``git`` et de le déposer sur un serveur ``Gitlab`` ou ``Github``. 

-   Cela peut se faire en ligne de commandes dans un terminal.
-   Cela peut se faire avec un éditeur de texte comme ``VS Code`` ou ``VS Codium`` après avoir installé les extensions dédiées.

Git en ligne de commandes
---------------------------

On crée un premier projet vide ne contenant qu'un seul fichier ``README.md``. Ce fichier est placé dans un dossier nommé ``demo_gitlab`` et contient:

.. code:: md

    # Créer un projet

    Ce projet a pour objectif de donner les principales étapes de la construction d'un site web à partir de fichiers sources se trouvant sur une machine personnelle.

On ouvre un terminal et on se place dans le dossier ``demo_gitlab`` que l'on souhaite déposer sur Gitlab.

Dans le terminal, on initialise le projet avec la commande :

.. code:: sh

    git init

Cette commande crée un dossier caché nommé ``.git`` qui va contenir tous les fichiers nécessaires au dépot sur Gitlab de notre projet.

Pour établir la connexion avec le serveur Gitlab de la forge, il faut lui ajouter les paramètres d'authentification:

.. code:: sh

    git config --global user.name "Nom Utilisateur"
    git config --global user.email "votre.email@example.com"

.. note::

    L'option ``--global`` configure globalement votre profil git qui sera utilisé pour tous vos dépots. Il est possible de configurer localement un compte pour un dépot particulier. Il suffit de remplacer l'option par ``--local``.

Notre projet contient un fichier. On doit l'indexer dans Git en utilisant la commande :

.. code:: sh

    git add .

.. note::

    Le point utilisé en fin de commande désigne le dossier dans lequel on est placé pour indexer les fichiers et dossiers qu'il contient. Ici, il s'agit du dossier ``demo_gitlab``.

L'étape suivante consiste à commiter, c'est à dire informer git que les fichiers indexés seront à déposer dans notre dépot sur Gitlab. Un commit s'accompagne d'un message qui nous informe sur la raison principale du commit. Par exemple s'il s'agit du premier commit ou d'une simple modification. La commande est la suivante :

.. code:: sh

    git commit -m "Premier commit"

Une confirmation des fichiers à commiter est affichée après exécution de la commande.

La prochaine étape consiste à relier notre projet local au dépôt distant sur Gitlab. Cela se fait avec la commande :

.. code:: sh

    git remote add origin https://oauth2:<jeton_acces>@forge.apps.education.fr/<utilisateur>/<nom_projet>.git

La dernière étape consiste à envoyer tous ses fichiers sur le dépôt Gitlab. On dit que l'on "pousse" les fichiers. La commande est la suivante:

.. code:: sh

    git push -u origin main (ou master)


Après cette dernière commande, on voit l'affichage des différentes fichiers envoyés vers notre dépôt Gitlab. En allant sur notre dépot Gitlab, on peut vérifier la présence du fichier ``README.md``.

.. figure:: ../img/depot_gitlab.png
    :align: center

Si on ajoute un nouveau fichier ou que l'on fait une modification d'un fichier sur notre dépot local, il faudra en informer le dépôt distant Gitlab. On utilise les commandes suivantes :

.. code:: sh

    git add .
    git commit -m "message de modifications"
    git push origin main (ou master)



