Déploiement sur Gitlab
========================

.. _Gitlab: https://gitlab.com
.. _`Forge Apps Education`: https://forge.apps.education.fr

Le dépôt d'un projet sur Gitlab nécessite d'avoir un compte sur Gitlab_ ou sur `Forge Apps Education`_.

Suite à cela, il devient possible de déposer un projet sur la plateforme et de le déployer sous forme de pages web.

La communication entre sa machine et le serveur peut se faire soit par le protocole ``https`` soit par le protocole ``ssh``. 

Créer un Jeton d'Accès Personnel
-----------------------------------

Pour faire simplement, on utilisera le protocole ``https``. Pour que ce soit possible, il est nécessaire de créer un **jeton d'accès personnel** ou un **PAT** pour "Personnal Acces Token" dans Gitlab.

Il faut se rendre dans les préférences de son compte et choisir l'entrée de menu "Jetons d'accès" pour créer ce jeton.

.. figure:: img/jeton_acces.png
    :align: center

Cliquer sur le bouton "Ajouter un nouveau jeton" puis renseigner les champs proposés.

-   Un nom pour le jeton (obligatoire)
-   Une date d'expiration. Vous pouvez laisser vide pour avoir une année entière.
-   Sélectionner les portées : api, read_api, read_repository et write_repository.

.. figure:: img/creer_jeton.png
    :align: center

Ensuite valider et surtout enregistrer bien ce jeton dans un fichier sur votre machine le temps de finir la configuration ou ne fermer pas l'onglet ! Après il ne sera pas possible de récupérer ce jeton et donc de l'utiliser. Il faudra refaire la manipulation.



.. toctree::
    :maxdepth: 1
    :hidden:

    content/configurer.rst
    content/deployer.rst