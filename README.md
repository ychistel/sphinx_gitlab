# Projet sphinx sur Gitlab

Un premier tutoriel qui permet de créer un projet créé avec le module Python Sphinx et son déploiement sur Gitlab.

Le déploiement sur Gitlab permet:

- de déposer sur un serveur externe les fichiers et dossiers de son projet réalisé sur sa machine personnelle.
- de créer un site web statique avec les contenus issus des fichiers et dossiers de son projet.

Le site web associé à ce projet se trouve à l'adresse [Projet sphinx sur Gitlab](https://ychistel.forge.apps.education.fr/sphinx_gitlab/)
